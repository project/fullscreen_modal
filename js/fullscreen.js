/**
* @file
*/

(function ($, Drupal) {

  Drupal.closeFullScreen = function($old_focus) {
    $('.fullscreen-wrapper').contents().unwrap();
    $('#fullscreen-modal').remove();
    $('body').removeClass('fullscreen-modal-active');
    $old_focus.focus();
  }

  Drupal.AjaxCommands.prototype.fullscreen = function (ajax, response, status) {
    let $modal = $('<div id="fullscreen-modal" tabindex="-1" class="fullscreen-modal"></div>');
    var $old_focus = $(':focus');
    $($modal).html(response.content);
    $('.close', $modal).on('click', function() {
      Drupal.closeFullScreen($old_focus);
    })
    $('.close', $modal).on('keyup', function(event) {
      if (event.key == 'Enter') {
        Drupal.closeFullScreen($old_focus);
      }
    })
    $(window).once('fullscreen-close').keyup(function(event) {
      if (event.which == 27) {
        Drupal.closeFullScreen($old_focus);
      }
    })
    Drupal.closeFullScreen($old_focus);
    $('body').wrapInner('<div class="fullscreen-wrapper" tabindex="0" role="presentation"</div>');
    for (var i = 0; i < response.dialog_options.wrapper_classes.length; i++) {
      $('.fullscreen-wrapper').addClass(response.dialog_options.wrapper_classes[i]);
    }
    $('body').addClass('fullscreen-modal-active');
    $('body').append($modal);
    $('#fullscreen-modal').focus();
    $('.fullscreen-wrapper').on('focus' , function() {
      $('#fullscreen-modal').focus();
    });
    Drupal.attachBehaviors($modal.get(0));
  }

})(jQuery, Drupal);
