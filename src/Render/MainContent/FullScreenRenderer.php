<?php

namespace Drupal\fullscreen_modal\Render\MainContent;

use Drupal\Core\Render\Renderer;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Template\Attribute;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Controller\TitleResolverInterface;
use Drupal\Core\Render\MainContent\DialogRenderer;
use Drupal\fullscreen_modal\Ajax\OpenFullScreenCommand;

/**
 * Default main content renderer for modal dialog requests.
 */
class FullScreenRenderer extends DialogRenderer {

  /**
   * Renderer service.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * Constructs a new DialogRenderer.
   *
   * @param \Drupal\Core\Controller\TitleResolverInterface $title_resolver
   *   The title resolver.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer service.
   */
  public function __construct(TitleResolverInterface $title_resolver, Renderer $renderer) {
    parent::__construct($title_resolver);
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public function renderResponse(array $main_content, Request $request, RouteMatchInterface $route_match) {
    $response = new AjaxResponse();

    // First render the main content, because it might provide a title.
    $content = $this->renderer->renderRoot($main_content);

    // Attach the library necessary for using the OpenModalDialogCommand and set
    // the attachments for this Ajax response.
    $main_content['#attached']['library'][] = 'fullscreen_modal/fullscreen';
    $response->setAttachments($main_content['#attached']);

    // If the main content doesn't provide a title, use the title resolver.
    $title = isset($main_content['#title']) ?
      $main_content['#title']
      : $this->titleResolver->getTitle($request, $route_match->getRouteObject());

    $options = $request->request->get('dialogOptions', [
      'wrapper_classes' => ['blur', 'opacity'],
    ]);

    $contentAttributes = new Attribute(
      $request->request->get('attributes', [
        'class' => [],
      ])
    );


    $build = [
      '#theme' => 'fullscreen_modal_content',
      '#title' => $title,
      '#content' => $content,
      '#attributes' => $contentAttributes,
    ];

    $wrapper = [
      '#theme' => 'fullscreen_modal_container',
      '#content' => $build,
    ];

    $wrapper = $this->renderer->renderRoot($wrapper);

    $response->addCommand(new OpenFullScreenCommand($wrapper, $options));
    return $response;
  }

}
