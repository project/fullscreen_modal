<?php

namespace Drupal\fullscreen_modal\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * Class OpenFullScreenCommand.
 */
class OpenFullScreenCommand implements CommandInterface {

  /**
   * Content to display.
   *
   * @var mixed
   */
  protected $content;

  /**
   * Modal options.
   *
   * @var array
   */
  protected $dialogOptions;

  /**
   * Settings.
   *
   * @var mixed
   */
  protected $settings;

  /**
   * Constructor.
   *
   * @param mixed $content
   *   Content to insert.
   * @param array $dialog_options
   *   Dialog options.
   * @param array $settings
   *   Settings.
   */
  public function __construct($content, array $dialog_options = [], array $settings = NULL) {
    $this->content = $content;
    $this->dialogOptions = $dialog_options;
    $this->settings = $settings;
  }

  /**
   * Render custom ajax command.
   *
   * @return array
   *   Command function.
   */
  public function render() {
    return [
      'command' => 'fullscreen',
      'content' => $this->content,
      'dialog_options' => $this->dialogOptions,
      'settings' => $this->settings,
    ];
  }

}
